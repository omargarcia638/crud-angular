import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Comentario } from '../../models/comentario';
import { ComentarioService } from '../../services/comentario.service';


@Component({
  selector: 'app-agregar-editar-comentario',
  templateUrl: './agregar-editar-comentario.component.html',
  styleUrls: ['./agregar-editar-comentario.component.css']
})
export class AgregarEditarComentarioComponent implements OnInit {
  comentarios: FormGroup;
  idComentarios =0;
  accion = 'Agregar';
  loading = false;
  comentario: Comentario;

  constructor(private fb: FormBuilder, private route: ActivatedRoute,
              private ComentarioService: ComentarioService, private router: Router) { 
    this.comentarios = this.fb.group({
      titulo: ['',Validators.required],
      creador:['',Validators.required],
      texto: ['',Validators.required],
    });
    if(+this.route.snapshot.paramMap.get('id') > 0){
      this.idComentarios = +this.route.snapshot.paramMap.get('id');
    }    
  }

  ngOnInit(): void {
    this.esEditar();
  }
  guardarComentario(){
    if(this.accion === 'Agregar'){
      const comentario: Comentario = {
        fechaCreacion: new Date(),
        creador: this.comentarios.get('creador').value,
        titulo: this.comentarios.get('titulo').value,
        texto: this.comentarios.get('texto').value,
      };
      this.ComentarioService.guardarComentario(comentario).subscribe(data =>{
        this.router.navigate(['/']);
      });      
    } else{
      const comentario: Comentario = {
        id: this.comentario.id,
        fechaCreacion: this.comentario.fechaCreacion,
        creador: this.comentarios.get('creador').value,
        titulo: this.comentarios.get('titulo').value,
        texto: this.comentarios.get('texto').value,
      };
      this.ComentarioService.actualizarComentarios(this.idComentarios, comentario).subscribe(data =>{
        this.router.navigate(['/']);
      });
    }
    console.log(this.comentarios);
  }

    esEditar(){
    if(this.idComentarios > 0) { 
      this.accion = 'Editar';
      this.ComentarioService.cargarComentario(this.idComentarios).subscribe(data =>{
        this.comentario = data;
        this.comentarios.patchValue({
          titulo: data.titulo,
          creador: data.creador,
          texto: data.texto
      });
      });
    }
  }
  
}
