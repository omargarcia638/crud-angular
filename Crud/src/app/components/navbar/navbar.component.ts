import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  urlImage = 'https://lh3.googleusercontent.com/proxy/TUmocKVCcfa5XcH_cfgOt2QDJH-eJTNgDxyjRxhnTx-FcrRkSErzWyNQtvDBuV_hMAzqTXT_WoMfkgcCjWdFesBKZYkA14ij_fZeAQ-QgVxl2kugwvE';
  constructor() { }

  ngOnInit(): void {
  }

}
